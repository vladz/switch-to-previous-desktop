## Installation

Run

```
kpackagetool6 --type KWin/Script --install package/
```

then go to "KWin Scripts" settings and enable "Switch to previous desktop" script.


## Activation

Press `Meta+Z` to switch to the previously used virtual desktop.


## Changing shortcut

By default `Meta+Z` is reserved by the script, but it can be changed in "Shortcuts" settings. Search for "Switch to Previously Used Desktop".
